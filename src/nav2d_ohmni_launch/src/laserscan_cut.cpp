#include <ros/ros.h>
#include <sensor_msgs/LaserScan.h>
#include <dynamic_reconfigure/server.h>

#include "nav2d_ohmni_launch/laserscan_cutConfig.h"

ros::Publisher pub;
static double min_cut_rad_ = -0.1;
static double max_cut_rad_ = 0.1;
static bool is_cut_ = false;
static bool is_flip_ = false;
void callback(const sensor_msgs::LaserScan::ConstPtr& msg)
{
  if(is_cut_) {
    int max_index = (msg->angle_max - msg->angle_min)/msg->angle_increment - 1;
    int index_min = (min_cut_rad_ - msg->angle_min)/msg->angle_increment;
    if (index_min < 0) index_min = 0; if(index_min > max_index) index_min = max_index;
    int index_max = (max_cut_rad_ - msg->angle_min)/msg->angle_increment;
    if (index_max < 0) index_max = 0; if(index_max > max_index) index_max = max_index;
    sensor_msgs::LaserScan cutted_ls = *msg;
    if(index_min <= index_max) {
      if (is_flip_) {
        for(int i = 0; i < index_min; i++) {
          cutted_ls.ranges.at(i) = std::numeric_limits<float>::infinity();
        }
        for(int i = index_max; i < cutted_ls.ranges.size(); i++) {
          cutted_ls.ranges.at(i) = std::numeric_limits<float>::infinity();
        }
      }
      else {
        for(int i = index_min; i <= index_max; i++) {
          cutted_ls.ranges.at(i) = std::numeric_limits<float>::infinity();
        }
      }
    }
    pub.publish(cutted_ls);
  }
  else {
    pub.publish(*msg);
  }
}

void Reconfigurecb(laserscan_cut::laserscan_cutConfig &config, uint32_t level) {
  ROS_INFO("Config cut angles: min %f, max %f", config.min_angle_cut, config.max_angle_cut);
  if(config.min_angle_cut > config.max_angle_cut  ||
     config.min_angle_cut > 3.14 || config.min_angle_cut < -3.14 ||
     config.max_angle_cut > 3.14 || config.max_angle_cut < -3.14) {
    ROS_INFO(" >bypass laser scan");
    is_cut_ = false;
  }
  else {
    is_cut_ = true;
    min_cut_rad_ = config.min_angle_cut;
    max_cut_rad_ = config.max_angle_cut;
  }
  is_flip_ = config.flip_cut;
}


int main(int argc, char **argv){

  ros::init(argc, argv, "laserscan_cut");
  ros::NodeHandle nh;
  ros::NodeHandle pnh ("~");

  pub = nh.advertise<sensor_msgs::LaserScan>("cutted_scan", 1000);
  ros::Subscriber sub = nh.subscribe("/scan", 1000, callback);

  dynamic_reconfigure::Server<laserscan_cut::laserscan_cutConfig> manual_cfg_server_(pnh);
  manual_cfg_server_.setCallback(&Reconfigurecb);

  ros::spin();

  return 0;
}
