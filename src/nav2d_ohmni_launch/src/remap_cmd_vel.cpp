#include <ros/ros.h>
#include <std_msgs/String.h>
#include <nav2d_operator/cmd.h>
#include <geometry_msgs/Twist.h>

ros::Publisher pub;

void callback(const geometry_msgs::Twist::ConstPtr& msg)
{
  nav2d_operator::cmd cmd;
  cmd.Velocity = msg->linear.x;
  cmd.Turn = msg->angular.z;
  //  cmd.Turn = msg->angular.z*(-1);
  //    if(msg->angular.z > 1)
  //      cmd.Turn = -1;
  //    else if(msg->angular.z < -1)
  //      cmd.Turn = 1;
  cmd.Mode = 0;
  pub.publish(cmd);
}

int main(int argc, char **argv){

  ros::init(argc, argv, "remapTopicVel");
  ros::NodeHandle nh;
  ros::NodeHandle pnh ("~");
  pub = nh.advertise<nav2d_operator::cmd>("cmd", 1000);
  ros::Subscriber sub = nh.subscribe("manual_cmd_vel", 1000, callback);
  ros::spin();

  return 0;
}
